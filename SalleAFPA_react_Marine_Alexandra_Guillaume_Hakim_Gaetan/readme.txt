Projet : Gestion de salle AFPA 

Auteur : Marine , Alexandra ,Ga�tan, Hakim, Guillaume



R�partitions des t�ches : 


Marine :    - Migration des controllers en REST controllers

	    - Structures des pages en react + appel des Web Services
 
	    - Mise en place de l'authentification avec JWT dans le Back et Front



Alexendra : - Mise en forme des pages en react

	    - Mise en place de la responsivit�

	    - Developpement de pages en react
	    - Pagination des pages type materiel, batiment, type salle (Back et Front)	



Guillaume :  - Mise en place du hachage du mot de passe et verification du mot de passe hacher lors de l'authentification
	     - Correction de la mise en page de certains elements cot� front 



Hakim : 



Ga�tan : - D�veloppement Front: Mise en page de certains formulaires de cr�ation
	 -> Cr�ation bouton, navbar, placement des �l�ments dans la page
	 - Cr�ation icone de d�connexion


Technologies et outils utilis�s : Eclipse / Java / Maven / Lombok / Hibernate / JEE / JWT / Spring / Spring Data JPA / Spring Ioc / Bcrypt 
 / Gitkraken / Bitbucket / JMerise / DBeaver / PostgreSql / Postman / Slack / Trello 
					/ PgAdmin 4 / Apache Tomcat / node js 

D�marche � suivre pour le d�mmarage de l'application:

- Base de donn�es : 

	1) Sur pgAdmin, cr�er un r�le de connexion.

   		 nom = admin 
  		 mot de passe = 1234 

	2) Sur pgAdmin cr�er une nouvelle base de donn�es (nom : gestionSalle) PostgreSQL(avec l'user admin)

	3) Executer le script "script_creation_et_insertion.sql"( qui se trouve dans le dossier Bdd)

- Partie Back :
	
	4) Placer le dossier resources (qui se touve dans le dossier Back) dans le dossier webapps du serveur TomCat(version 9)
	   Placer le fichier salleafpa.war (qui se touve dans le dossier Back) dans le dossier webapps du serveur TomCat(version 9)
	   (pour que la sauvegarde et l'affichage des images fonctionnent il faut que le chemin du serveur TomCat soit : C:\ENV\serveurs\apache-tomcat-9.0.30\ )

	5) Demarrer le serveur TomCat (version 9) sur le port 8080 (ex�cuter le startup.bat du dossier bin du serveur TomCat)-

- Partie Front
		1) ouvrir l'invite de commandes se placer dans le dossier front/gestionsalle 

		2) Ex�cuter la commande npm insall

		3) Ex�cuter la commande npm start

	L'application est lanc�e, pour y acc�der l'url est http://localhost:3000/,  pour ce connecter vous pouvez utiliser le login admin et mot de passe : 1234

Une fois l'application lanc�e vous allez pouvoir acceder � la page d'authentification utilisateur :
	
	Vous allez pouvoir :	Cr�er, modifier, rechercher desactiver ou activer les salles de cours.
				Cr�er, modifier, rechercher et supprimer des b�timents.
				Cr�er, modifier, rechercher et supprimer du materiels.	
				Cr�er, modifier, rechercher et supprimer des r�servations.
	
	Vous pouvez aussi acc�der � la page d'authentification de l'administrateur, soit en cliquant sur le lien connexion administrateur soit en ajoutant /admin � l'url
	
	Vous pourrez alors : Cr�er, modifier, rechercher et supprimer des utilisateurs.	
 
				

	