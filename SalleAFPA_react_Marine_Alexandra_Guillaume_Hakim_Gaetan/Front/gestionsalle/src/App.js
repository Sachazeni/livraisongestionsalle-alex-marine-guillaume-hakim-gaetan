import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,Redirect,
} from "react-router-dom";
import { ToastProvider } from 'react-toast-notifications'

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'mdbreact/dist/css/mdb.css';

import FormCreationUser from './Pages/AdminNewUtilisateur'
import FormModificationUser from './Pages/AdminModifUtilisateur'
import Login from './Components/Login';
import LoginAdmin from './Components/LoginAdmin';
import ListeUsers from './Pages/AdminListeUtilisateurs';
import ListeSalle from './Pages/PageListeSalle';
import PageDivers from './Pages/PageDivers';
import CreationSalle from './Pages/PageCreationSalle';
import CreationReservation from './Pages/PageNewReservation';
import SalleDetails from './Pages/PageSalleDetails';
import PageListeReservation from './Pages/PageListeReservation';

import 'bootstrap-css-only/css/bootstrap.min.css';
import './css/App.css';
import './css/Authentification.css';
import './css/cssListUser.css';
import './css/cssFiltres.css';
import './css/cssListeSalles.css';
import './css/cssDivers.css';
function App() {
  return (
     
     <Router>
      
      <ToastProvider placement="bottom-right">
       {/* A <Switch> looks through its children <Route>s and
           renders the first one that matches the current URL. */}
       <Switch>
       
         <PrivateRoutePartieAdmin path="/admin/user/modif/:login">
           <FormModificationUser />
         </PrivateRoutePartieAdmin>
         <PrivateRoutePartieAdmin path="/admin/user/new">
           <FormCreationUser />
         </PrivateRoutePartieAdmin>

         <PrivateRoutePartieAdmin path="/admin/user/:page" >
         <ListeUsers />
         </PrivateRoutePartieAdmin>

         <PrivateRoutePartieAdmin path="/admin/user">
           <ListeUsers />
         </PrivateRoutePartieAdmin>

         <PrivateRoutePartieAdmin path="/fonction/:page">
           <PageDivers type="fonction"/>
         </PrivateRoutePartieAdmin>
         <PrivateRoutePartieAdmin path="/fonction">
           <PageDivers type="fonction"/>
         </PrivateRoutePartieAdmin>

         <Route path="/admin" >
           <LoginAdmin />
           </Route>
           <PrivateRoute path="/salle/:id" >
           <SalleDetails />
         </PrivateRoute>
         <PrivateRoute path="/listesalle/:page" >
           <ListeSalle />
         </PrivateRoute>

         <PrivateRoute path="/listesalle" >
           <ListeSalle />
         </PrivateRoute>
         <PrivateRouteAdmin path="/typemateriel/:page" >
         <PageDivers type="typemateriel" />
         </PrivateRouteAdmin>
         <PrivateRouteAdmin path="/typemateriel" >
         <PageDivers type="typemateriel" />
         </PrivateRouteAdmin>
         
        
         <PrivateRouteAdmin path="/listereservaton/:page" >
         <PageListeReservation />
         </PrivateRouteAdmin>
         <PrivateRouteAdmin path="/listereservaton" >
         <PageListeReservation />
         </PrivateRouteAdmin>

         <PrivateRouteAdmin path="/batiment/:page" >
           <PageDivers type="batiment" />
         </PrivateRouteAdmin>
         <PrivateRouteAdmin path="/batiment" >
           <PageDivers type="batiment" />
         </PrivateRouteAdmin>

         <PrivateRouteAdmin path="/typesalle/:page" >
         <PageDivers type="typesalle" />
         </PrivateRouteAdmin>
         <PrivateRouteAdmin path="/typesalle" >
         <PageDivers type="typesalle" />
         </PrivateRouteAdmin>

         <PrivateRouteAdmin path="/creationsalle" >
         <CreationSalle/>
         </PrivateRouteAdmin>
         <PrivateRouteAdmin path="/creationreservation" >
         <CreationReservation/>
         </PrivateRouteAdmin>
         <Route path="/" >
           <Login />
         </Route>
       </Switch>
      </ToastProvider>
   </Router>
  );


  function PrivateRoute({ children, ...rest }) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          sessionStorage.token  && sessionStorage.partie==="salle" ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }
}

function PrivateRouteAdmin({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
      sessionStorage.token && sessionStorage.admin==="true"  && sessionStorage.partie==="salle"? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/salle",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}
function PrivateRoutePartieAdmin({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
      sessionStorage.token && sessionStorage.admin==="true" && sessionStorage.partie==="admin" ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/admin",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}


export default App;
