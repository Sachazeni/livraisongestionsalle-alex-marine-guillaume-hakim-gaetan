import React,{useState} from 'react';
import Header from '../Components/Header'
import HeaderAdmin from '../Components/HeaderAdmin'
import TableauDivers from '../Components/TableauDivers'

export default function PageDivers({type}){

    const [refresh,setRefresh]=useState(false);

    const modif = function(){
        setRefresh(!refresh)
    }

    return <div>
        {type==="fonction" ?(<HeaderAdmin/>) : (<Header/>)}
        
        <TableauDivers type={type} refresh={refresh} setRefresh={modif} />
        
  
    </div>
}