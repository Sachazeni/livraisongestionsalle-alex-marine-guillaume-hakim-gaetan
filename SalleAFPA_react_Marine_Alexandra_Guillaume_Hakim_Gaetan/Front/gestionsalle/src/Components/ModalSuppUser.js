import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import {useHistory} from 'react-router-dom';


export default function FormSuppressionUser({ user, show, setShow }) {
    const history = useHistory();



    function toggle() {
        setShow(!show);
        history.push('/admin/user')
    }

    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={toggle}></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={toggle}>
                    <MDBModalHeader toggle={toggle}>
                        Suppression
                    </MDBModalHeader>
                    <MDBModalBody>
                        Etes vous sûr de vouloir supprimer<br />
                        {user.nom} {user.prenom}<br />
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn onClick={toggle}>Oui</MDBBtn>
                        <MDBBtn onClick={toggle}>Non</MDBBtn>

                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>);

}