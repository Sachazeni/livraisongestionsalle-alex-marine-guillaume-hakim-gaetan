import React, { Fragment,useEffect }  from 'react';
import $ from 'jquery'
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter ,MDBRow, MDBInput} from 'mdbreact';

export default function ModalModifReservations({setMessagesErreur, reservation, show, setShow,modification,messagesErreur }) {
 

     function toggle(reponse) {
       
        if(reponse){
            let reservationModif = {
                "id":reservation.id,
                "dateDebut":$("#dateDebut").val(),
                "dateFin":$("#dateFin").val(),
                "nomReservation":$("#nom").val(),
                "salle":reservation.salle
        }
           modification(reservationModif);
        }else{
            setShow(false);
        }
    }

    


      useEffect(() => {
        setMessagesErreur([])
    }, [show]);


    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={toggle} ></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={()=>toggle(false)} centered text-center> 
                    <MDBModalHeader toggle={()=>toggle(false)} text-center>
                    <h3 className='text-center'>Modification de la Réservation</h3>
                    </MDBModalHeader>
                    <MDBModalBody className="container " text-center>
                    <p className="text-center">Modifier :</p><br/>
                    <em  className="text-center red-text" >{messagesErreur.reservationKO}</em> 
                    
                    <form
      className="needs-validation"
      noValidate
    >
     
      
          <MDBInput
            valueDefault={reservation.nomReservation}
            name="nomReservation"
            type="text"
            id="nom"
            label="Nom"
            required
          >
            <em  className="text-center red-text" >{messagesErreur.nomKO}</em>
          </MDBInput>
          
        <MDBInput
            valueDefault={reservation.dateDebut}
            name="dateDebut"
            type="date"
            id="dateDebut"
            label="Date de debut"
            
            disabled={new Date(reservation.dateDebut)<new Date() ? true : false}
            required>
          </MDBInput>
          
        <MDBInput
            valueDefault={reservation.dateFin}
            name="dateFin"
            type="date"
            id="dateFin"
            label="Date de fin"
           
            required>
               <em  className="text-center red-text" >{messagesErreur.dateKO}</em>
          </MDBInput>

    </form> 

                    
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn className="btnGeneralDark" onClick={()=>toggle(true)}>Modifier</MDBBtn>
                        <MDBBtn className="btnGeneralBlanc" onClick={()=>toggle(false)}>Retour</MDBBtn>

                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>);

}