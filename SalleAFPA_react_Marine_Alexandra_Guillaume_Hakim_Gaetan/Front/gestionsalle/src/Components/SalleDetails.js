import React, { useState, useEffect,Fragment } from 'react';
import { MDBRow, MDBCol, MDBBtn, MDBInput, MDBFormInline, MDBContainer } from "mdbreact";
import {Link,useHistory} from 'react-router-dom';
import {useParams} from 'react-router-dom'
import {  useToasts } from 'react-toast-notifications';
import ModalSalle from './ModalConfirmationCreationModifSalle.js';
import '../css/cssDetailsSalle.css'

export default function SalleDetails() {
  const [salle, setSalle] = useState({});
  const [typeSalles, setTypeSalles] = useState([]);
  const [typeMateriels, setTypeMateriels] = useState([]);
  const [batiments, setBatiments] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [erreur, setErreur] = useState(true);
  const { addToast } = useToasts();
  const [show, setShow] = useState(false);
  const [reponseServeur, setReponseServeur] = useState({});

  const [messagesErreur, setMessagesErreur] = useState([]);

  const { id } = useParams();
  const history = useHistory();

  const [listeReservation, setListeReservation] = useState([]);

  const getSalle = async () => {
    let response = await fetch(`${process.env.REACT_APP_API_URL}/salle/${id}`, {
      headers: {
          'Authorization': sessionStorage.token
      }
  })
      .catch(error => console.log(error))

      if (response && response.status===200) {
         let responseData = await response.json();
        setSalle(responseData);
        setErreur(false)
        setListeReservation(responseData.listeReservation)
    }else if(response && response.status===401){
        addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/') }})
       
      } else {
        addToast("salle non trouvée", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000 })
        history.push('/salle')
    }

  }

  const getypeSalle = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/typesalle`, {
      headers: {
          'Authorization': sessionStorage.token
      }
  })
      .then(response => response.json())
      .then(data => setTypeSalles(data))
      .catch(error => console.log(error))
  }

  const getTypeMateriel = async () => {
    await fetch(`${process.env.REACT_APP_API_URL}/typemateriel`, {
      headers: {
          'Authorization': sessionStorage.token
      }
  })
      .then(response => response.json())
      .then(data => setTypeMateriels(data))
      .catch(error => console.log(error))
  }

  const getBatiment = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/batiment`, {
      headers: {
          'Authorization': sessionStorage.token
      }
  })
      .then(response => response.json())
      .then(data => setBatiments(data))
      .catch(error => console.log(error))
  }

  function changeHandler(event) {
    let salleTemp = salle
    if (event.target.name === "typeSalle") {
      salleTemp.typeSalle = typeSalles.find(e => e.id == event.target.value)
    } else if (event.target.name === "batiment") {
      salleTemp.batiment = batiments.find(e => e.id == event.target.value)
    } else if (event.target.name === "typeMateriel") {
      let typeMateriel = typeMateriels.find(e => e.id == event.target.id)
      var  materiel=salle.listeMateriel.find((m)=>m.typeMateriel.id==typeMateriel.id);
      if(materiel){
        materiel.quantite=event.target.value;
      }else{
        materiel={};
        materiel.typeMateriel=typeMateriel;
        materiel.quantite=event.target.value;
        materiel.id=0;
      }
    salleTemp.listeMateriel=salleTemp.listeMateriel.filter((m)=>{
      return m.typeMateriel.id!=materiel.typeMateriel.id
    })
    salleTemp.listeMateriel.push(materiel)
    }else  if(event.target.name === "actif"){
      salleTemp.actif=event.target.value
    }else {
      salleTemp[event.target.name] = event.target.value;
    }
    setSalle(salleTemp)
    
  };



  async function submitForm(event) {
    event.preventDefault();
    let response = await fetch(`${process.env.REACT_APP_API_URL}/salle`, {
      method: "PUT",

      body: JSON.stringify(salle),

      headers: {
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8",
        'Authorization': sessionStorage.token
      }
    })

 

    if (response.status===200) {
      let responseData = await response.json();
      setReponseServeur(responseData)

      setShow(true);
  }else if(response.status===401){
      addToast("authentification requise", { appearance: 'error' ,autoDismiss: true })
      history.push('/')
    } else {
      addToast("la modification a échouée", { appearance: 'error',autoDismiss: true  })
      let responseData = await response.json();
      setMessagesErreur(responseData)
  }
    
}

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      await  getSalle();
      await getTypeMateriel();
      await getypeSalle();
      await getBatiment();
     
      setIsLoading(false);
    }
    fetchData()
   
   


  }, []);



  const batimentsList = batiments.map((batiment) => <option key={batiment.id} value={batiment.id} selected={batiment.id===salle.batiment.id} >{batiment.libelle}</option>);
  const typesallesList = typeSalles.map((typeSalle) => <option key={typeSalle.id} value={typeSalle.id} selected={typeSalle.id===salle.typeSalle.id} >{typeSalle.libelle}</option>);
  const typeMaterielsList = typeMateriels.map((typeMateriel) =>{

    let erreurSaisie;
    if(salle){
  var materiel = salle.listeMateriel.find(m=>m.typeMateriel.id===typeMateriel.id);
  
  if(!materiel){
     materiel={};
    materiel.quantite=0;
  }else{
    if(materiel.quantite<0){
      erreurSaisie="*Erreur de saisie: doit être un chiffre positif"
    }
  }

    return <MDBRow key={typeMateriel.id}> <div className="form-group">
      <label htmlFor="typeMateriel">{typeMateriel.libelle}</label>
      <input
        type="number"
        className="form-control"
        name="typeMateriel"
        id={typeMateriel.id}
        onChange={changeHandler}
        defaultValue={materiel.quantite}
      />
      <em  className="text-center red-text" >{erreurSaisie}</em>
    </div></MDBRow>}
  });

  const reservationList = listeReservation.map((reservation) => {
return <tr><td >{reservation.nomReservation}</td>
<td></td>
        <td  >{reservation.dateDebut}</td>
        <td></td>
        <td  >{reservation.dateFin}</td>    
    </tr>})
  

  

  
  return <Fragment>
  {isLoading || erreur ? (
    <div></div>
  ) : (<div className="newSalle">
    <h3 style={{textAlign:"center", fontWeight:"bold", fontStyle:"italic"}} >Informations de la salle : </h3>
    <br/>
    <br/>
    <form
      className="needs-validation formSalle"
      onSubmit={submitForm}
      noValidate
    >
    <MDBContainer>
      <MDBRow>
        <MDBCol md="2">
          <br />
          <br />
          
          
          


        <MDBInput
              valueDefault={salle.nom}
              name="nom"
              onChange={changeHandler}
              type="text"
              id="nom"
              label="Nom"
              required
            >
              <em  className="text-center red-text" >{messagesErreur.nomKO}</em>
         </MDBInput>

         <label
              htmlFor="typeSalle"
              className="grey-text"
            >
              Type de salle :
              </label>
            <select className="browser-default custom-select" name="typeSalle" onChange={changeHandler}>
              
              {typesallesList}
            </select>
            <label
              htmlFor="typeSalle"
              className="grey-text"
            >
              Batiment :
              </label>
            <select className="browser-default custom-select" name="batiment" onChange={changeHandler}>
             
              {batimentsList}
            </select>

            <MDBInput
              valueDefault={salle.numero}
              name="numero"
              onChange={changeHandler}
              type="number"
              id="numero"
              label="Numéro"
              required
            >
               <em  className="text-center red-text" >{messagesErreur.numeroKO}</em>
            </MDBInput>

            <MDBInput
              valueDefault={salle.etage}
              name="etage"
              onChange={changeHandler}
              type="number"
              id="etage"
              label="Etage"
              required
            >
               <em  className="text-center red-text" >{messagesErreur.etageKO}</em>
            </MDBInput>

            <MDBInput
              valueDefault={salle.capacite}
              name="capacite"
              onChange={changeHandler}
              type="number"
              id="capacite"
              label="Capacité"
              required
            >
              <em  className="text-center red-text" >{messagesErreur.capaciteKO}</em>
            </MDBInput>

            <MDBInput
              valueDefault={salle.surface}
              name="surface"
              onChange={changeHandler}
              type="number"
              id="superficie"
              label="Superficie"
              required
            >
              <em  className="text-center red-text" >{messagesErreur.surfaceKO}</em>
            </MDBInput>

            <h5>Salle active/inactive :</h5>

            <MDBFormInline>
              <MDBInput
               onChange={changeHandler}
                label='active'
                type='radio'
                id='active'
                name="actif"
                value="true"
                containerClass='mr-5'
                defaultChecked={salle.actif}
              />
              <MDBInput
               onChange={changeHandler}
                label='inactive'
                type='radio'
                id='inactive'
                name="actif"
                value="false"
                defaultChecked={!salle.actif}

                containerClass='mr-5'
              />
            </MDBFormInline>
        </MDBCol>
        <MDBCol lg="1" md="1"></MDBCol>
        <MDBCol md="3">
        <h4>Liste de matériels de la salle :</h4>
        <br/>
        <br/>
        <div className=" d-flex align-items-center flex-column scrollbar scrollbar-primary">
       

        {typeMaterielsList}

        </div>

        </MDBCol>
        <MDBCol lg="1" md="1"></MDBCol>
        <MDBCol>
        <h4>Liste des réservations :</h4>
        <br/>
        <br/>
        
        <div class="table  table-responsive-lg">
        <table>
          <thead class="thead-dark">
            <tr >
              <th  fontWeight="bold" scope="col">Nom réservation</th>
            <th></th>
              <th fontWeight="bold" scope="col">Date de début</th>
              <th></th>
              <th fontWeight="bold" scope="col">Date de fin</th>
            </tr>

          </thead>
      {reservationList}
      </table>
      </div>
        </MDBCol>
      </MDBRow>
      <MDBRow>
        <MDBCol md="5"></MDBCol>
        <MDBCol>
      <MDBBtn  class="btn  btnValiderDetails fa fa-check" type="submit"> Valider</MDBBtn>
      </MDBCol>
      </MDBRow>
    </MDBContainer>
    </form>
    <ModalSalle typeEvent={2} salle={reponseServeur} show={show} setShow={setShow}/>
  </div>)}
  </Fragment>
}