import React, { Fragment }  from 'react';

import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

export default function FormSuppressionUser({ reservation, show, setShow,suppr }) {




    function toggle(reponse) {
        setShow(false);
        if(reponse){
            suppr();
        }
    }

    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={toggle}></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={toggle} centered> 
                    <MDBModalHeader toggle={toggle}>
                       Confirmation de suppression
                    </MDBModalHeader>
                    <MDBModalBody>
                        Etes vous sûr de vouloir supprimer la réservation : <br />
                        {reservation.nomReservation} du  {new Date(reservation.dateDebut).toLocaleDateString('fr-FR')} au {new Date(reservation.dateFin).toLocaleDateString('fr-FR')}<br />
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn className="btnGeneralBlanc" onClick={()=>toggle(true)}>Oui</MDBBtn>
                        <MDBBtn className="btnGeneralDark" onClick={()=>toggle(false)}>Non</MDBBtn>

                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>);

}