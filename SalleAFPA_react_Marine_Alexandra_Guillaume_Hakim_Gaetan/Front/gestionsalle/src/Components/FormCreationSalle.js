import React, { useState, useEffect, useRef } from 'react';
import { MDBRow, MDBCol, MDBBtn, MDBInput, MDBFormInline, MDBContainer } from "mdbreact";
import { useHistory } from 'react-router-dom';
import ModalSalle from './ModalConfirmationCreationModifSalle.js';
import { useToasts } from 'react-toast-notifications';

export default function FormCreationSalle() {
  const [salle, setSalle] = useState({
    "idSalle": "0",
    "numero": "",
    "nom": "",
    "surface": "",
    "capacite": "",
    "etage": "",
    "actif": true,
    "typeSalle": {
      "id": "",
      "libelle": ""
    },
    "listeReservation": [],
    "listeMateriel": [],
    "batiment": {
      "id": "",
      "nom": ""
    },
    "cheminImage": ""
  });


  const [typeSalles, setTypeSalles] = useState([]);
  const [typeMateriels, setTypeMateriels] = useState([]);
  const [batiments, setBatiments] = useState([]);
  const [reponseServeur, setReponseServeur] = useState({});
  const [show, setShow] = useState(false)
  const { addToast } = useToasts();
  const history = useHistory();
  const image = useRef(null);

  const [labelInputImage, setLabelInputImage] = useState("Choisir une image")

  const getypeSalle = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/typesalle`, {
      headers: {
        'Authorization': sessionStorage.token
      }
    })
      .then(response => response.json())
      .then(data => setTypeSalles(data))
      .catch(error => console.log(error))
  }

  const getTypeMateriel = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/typemateriel`, {
      headers: {
        'Authorization': sessionStorage.token
      }
    })
      .then(response => response.json())
      .then(data => setTypeMateriels(data))
      .catch(error => console.log(error))
  }

  const getBatiment = async () => {
    fetch(`${process.env.REACT_APP_API_URL}/batiment`, {
      headers: {
        'Authorization': sessionStorage.token
      }
    })
      .then(response => response.json())
      .then(data => setBatiments(data))
      .catch(error => console.log(error))
  }

  function changeHandler(event) {
    let salleTemp = salle
    if (event.target.name === "typeSalle") {
      salleTemp.typeSalle = typeSalles.find(e => e.id == event.target.value)
    } else if (event.target.name === "batiment") {
      salleTemp.batiment = batiments.find(e => e.id == event.target.value)
    } else if (event.target.name === "typeMateriel") {
      let typeMateriel = typeMateriels.find(e => e.id == event.target.id)
      var materiel = {};
      materiel.typeMateriel = typeMateriel;
      materiel.quantite = event.target.value;
      materiel.id = 0;
      salleTemp.listeMateriel = salleTemp.listeMateriel.filter((m) => {
        return m.typeMateriel.id != materiel.typeMateriel.id
      })
      salleTemp.listeMateriel.push(materiel)
    } else if (event.target.name === "actif") {
      salleTemp.actif = event.target.value
    }
    else if (event.target.name === "file") {
setLabelInputImage(image.current.files[0].name)
    } else {
      salleTemp[event.target.name] = event.target.value;
    }
    setSalle(salleTemp)

  };



  async function submitForm(event) {
    event.preventDefault();


    const toSend = new FormData();
    toSend.append('image', image.current.files[0]);

    let response = await fetch(`${process.env.REACT_APP_API_URL}/image`, {
      method: "POST",
      body: toSend,

    }).catch(error => console.log(error));
    if (response && response.status === 200) {
      let responseData = await response.json();
      console.log(responseData)
      salle.cheminImage = responseData.cheminImage
    }

    response = await fetch(`${process.env.REACT_APP_API_URL}/salle`, {
      method: "POST",
      body: JSON.stringify(salle),

      headers: {
        //"Content-Type": "multipart/form-data" }
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8",
        'Authorization': sessionStorage.token
      }

    }).catch(error => console.log(error));
    if (response && response.status === 200) {
      let responseData = await response.json();
      setReponseServeur(responseData);
      setShow(true)


    } else if (response && response.status === 400) {
      let responseData = await response.json();
      setReponseServeur(responseData);
    } else if (response && response.status === 401) {
      addToast("authentification requise", { appearance: 'error', autoDismiss: true, autoDismissTimeout: 1000, onDismiss: (id) => { history.push('/') } })

    } else {
      addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
    }

  }


  
  useEffect(() => {
    getTypeMateriel();
    getypeSalle();
    getBatiment();
  }, []);



  const batimentsList = batiments.map((batiment) => <option key={batiment.id} value={batiment.id} >{batiment.libelle}</option>);
  const typesallesList = typeSalles.map((typeSalle) => <option key={typeSalle.id} value={typeSalle.id} >{typeSalle.libelle}</option>);
  const typeMaterielsList = typeMateriels.map((typeMateriel) =>
    <MDBRow key={typeMateriel.id}>
      <div className="form-group justify-content-center">
        <label htmlFor="typeMateriel">{typeMateriel.libelle}</label>
        <input
          type="number"
          className="form-control"
          name="typeMateriel"
          id={typeMateriel.id}
          onChange={changeHandler}
        />

      </div></MDBRow>);

  return <div className="newSalle">
   
    <form
      className="needs-validation formSalle"
      onSubmit={submitForm}
      noValidate
    >
       <h3 className="text-center" >Formulaire Nouvelle Salle : </h3>
       <hr/>
      <MDBContainer className="mt-5">
        <MDBRow>
          <MDBCol lg="3" md="10" className="mb-4"></MDBCol>
          <MDBCol lg="5" md="6" className="mb-4" >
            <div className="input-group">
              <div className="input-group-prepend">
                
              </div>
              <div className="custom-file">
                <input
                  type="file"
                  className="custom-file-input"
                  id="inputGroupFile01"
                  aria-describedby="inputGroupFileAddon01"
                  accept="image/*"
                  ref={image}
                  name="file"
                  onChange={changeHandler}
                
                />
                <label className="custom-file-label" htmlFor="inputGroupFile01" >
             {labelInputImage}
                </label>
              </div>
            </div>

          </MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol md="6" className="mb-0">
            <MDBRow >
              <MDBInput
                valueDefault={salle.nom}
                name="nom"
                onChange={changeHandler}
                type="text"
                id="nom"
                label="Nom"
                required
              >
                <em className="text-center red-text" >{reponseServeur.nomKO}</em>
              </MDBInput>
            </MDBRow>

            <MDBRow>
              <label
                htmlFor="typeSalle"
                className="grey-text"
              >
                Type de salle :
              </label>
              <select className="browser-default custom-select" name="typeSalle" onChange={changeHandler}>
                <option>Type de salle</option>
                {typesallesList}
              </select>
              <em className="text-center red-text" >{reponseServeur.typeSalleKO}</em>

            </MDBRow>
            <MDBRow>
              <label
                htmlFor="typeSalle"
                className="grey-text"
              >
                Batiment :
              </label>
              <select className="browser-default custom-select" name="batiment" onChange={changeHandler}>
                <option>Choix batiment</option>
                {batimentsList}
              </select>
              <em className="text-center red-text" >{reponseServeur.batimentKO}</em>

            </MDBRow>
            <MDBRow >
              <MDBInput
                valueDefault={salle.numero}
                name="numero"
                onChange={changeHandler}
                type="number"
                id="numero"
                label="Numéro"
                required
              >
                <em className="text-center red-text" >{reponseServeur.numeroKO}</em>
              </MDBInput>
            </MDBRow>
            <MDBRow >
              <MDBInput
                valueDefault={salle.etage}
                name="etage"
                onChange={changeHandler}
                type="number"
                id="etage"
                label="Etage"
                required
              >
                <em className="text-center red-text" >{reponseServeur.etageKO}</em>
              </MDBInput>
            </MDBRow>
            <MDBRow >
              <MDBInput
                valueDefault={salle.capacite}
                name="capacite"
                onChange={changeHandler}
                type="number"
                id="capacite"
                label="Capacité"
                required
              >
                <em className="text-center red-text" >{reponseServeur.capaciteKO}</em>
              </MDBInput>
            </MDBRow>
            <MDBRow >
              <MDBInput
                valueDefault={salle.surface}
                name="surface"
                onChange={changeHandler}
                type="number"
                id="superficie"
                label="Superficie"
                required
              >
                <em className="text-center red-text" >{reponseServeur.superficieKO}</em>
              </MDBInput>
            </MDBRow>
            <MDBRow> Salle active / inactive : </MDBRow>
            <MDBRow>
              <MDBFormInline>
                <MDBInput
                  onChange={changeHandler}
                  label='active'
                  type='radio'
                  id='active'
                  name="actif"
                  value="true"
                  containerClass='mr-5'
                  checked
                />
                <MDBInput
                  onChange={changeHandler}
                  label='inactive'
                  type='radio'
                  id='inactive'
                  name="actif"
                  value="false"
                  containerClass='mr-5'
                />
              </MDBFormInline>
            </MDBRow>
          </MDBCol>
         


          <MDBCol md="6" className="mb-0">
            <br />
            <h3 className="text-center">Liste de matériels de la salle :</h3>
            <em className="text-center red-text" >{reponseServeur.qtiteKO}</em>
            <br />
            <div className="col-md-12 d-flex align-items-center flex-column scrollbar scrollbar-primary ">

              {typeMaterielsList}

            </div>
          </MDBCol>
        </MDBRow>

        <MDBRow className=" justify-content-end">
          <div className="col-sm-4 col-md-5">
           <MDBBtn id="btnValid" className="btn  btn-block btnGeneralDark" type="submit">Valider</MDBBtn>
           </div>
         </MDBRow>
      </MDBContainer>
    </form>
    <ModalSalle typeEvent={1} salle={reponseServeur} show={show} setShow={setShow} />
  </div>
}