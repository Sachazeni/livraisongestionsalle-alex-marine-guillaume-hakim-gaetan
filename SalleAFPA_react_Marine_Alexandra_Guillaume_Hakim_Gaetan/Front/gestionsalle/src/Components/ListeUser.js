import React, { useState, useEffect } from 'react';
import {  MDBBtn, MDBIcon } from 'mdbreact';
import {  useToasts } from 'react-toast-notifications';
import {useParams,useHistory, Link} from 'react-router-dom';
import User from './User';

export default function ListeUsers() {
    const [users, setUsers] = useState([]);
    const [suppr, setSuppr] = useState([]);
    const [nbPage, setNbPage] = useState(0);
    const { addToast } = useToasts();
    const history = useHistory();

    const { page } = useParams();
    const[pageUrl, setPageUrl] = useState( page ? page : 1);

    const getUsers = async () => {
        let pageUrl2 = page ? page : 1; 
       let response = await fetch(`${process.env.REACT_APP_API_URL}/personnepage/${pageUrl2}`, {
            headers: {
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8",
                'Authorization': sessionStorage.token
            }
        }).catch(error => {console.log(error);}); 
        console.log(response)
        if (response && response.status===200) {
            let responseData = await response.json();
            console.log(responseData)
            setUsers(responseData.listeUsers);
            setNbPage(responseData.nbPage);
        }else if(response && response.status===401){
            addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push("/");}})
           
          } else {
            addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
        }
    }

    useEffect(() => {
        setPageUrl(page ? page : 1)
        getUsers();
    }, [suppr,page]);

    function suppression() {
        setSuppr(!suppr);
    }
    
    const usersList = users.map((user) => <User key={user.id} user={user} suppression={suppression}/>);


    const pagination = () =>{
        let retour=[];
        for(let i=1;i<=nbPage;i++){
            retour.push(<li className={"page-item " + (i==pageUrl ? 'active' : '')} ><a className="page-link"onClick={ ()=>history.push("/admin/user/"+i)}>{i}</a></li>)
        }
        return retour;
    }
    

    return <div className="userCont container-fluid">
      <div className="row no-gutter">
      <div className="containerUserList col-md-3 d-none d-md-flex bg-imageUser align-items-center"></div>
        <div className="containerUserList col-md-8 col-lg-7 "> 
    <div className="userListContainer">
        <div className="headerContainer">
        <h3 className=" text-center">Liste des Utilisateurs :</h3>
        <hr/>
 
        
        </div>
        
            
        <br/>
        <table className="table table-sm table-striped table-hover">
            <thead className="theadUserContainer" ><tr>
                <th className="thdUser h5 text-center" scope="col">Login</th>
                <th className="thdUser h5 text-center" scope="col">Nom</th>
                <th className="thdUser h5 text-center" scope="col">Prenom</th>
                <th className="thdUser h5 text-center" scope="col">Mail</th>
                <th className="thdUser h5 text-center" scope="col">Telephone</th>
                <th className="thdUser h5 text-center" scope="col">Fonction</th>
                <th className="thdUser h5 text-center" scope="col" >Role</th>
                <th className="thdUser text-center " colSpan="2"><i className="fas fa-cogs"></i></th></tr>
            </thead>
            <tbody>{usersList}</tbody>

        </table>
        <nav aria-label="...">
            <ul className="pagination justify-content-center">
                <li className={"page-item " + ((pageUrl-1)<1 ? 'disabled' : '')}>
                <Link className="page-link" to={"/admin/user/"+(pageUrl-1)} >&laquo;</Link>
                </li> 
                {pagination()}
                <li className={"page-item " + ((parseInt(pageUrl)+1)>nbPage ? 'disabled' : '')}>
                    <Link className="page-link" to={"/admin/user/"+(parseInt(pageUrl)+1)}>&raquo;</Link>
                </li>
            </ul>
        </nav>
    </div>
    </div>
    </div>
    </div>;
}