import React from 'react';
import Salle from './Salle';

export default function TAbleauSalle({salles}) {
    
    const salleListe = salles.map((salle) => <Salle key={salle.id} salle={salle}/>);

    return <div className="listeSalle">
        <h3 className="text-center">Liste des salles :</h3> 
        <hr/>
        <br/>
        <div className="card-columns">
        {salleListe}
        </div>
        </div>;
}