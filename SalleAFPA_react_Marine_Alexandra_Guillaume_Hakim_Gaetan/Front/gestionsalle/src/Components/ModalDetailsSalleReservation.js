import React, { Fragment } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

export default function ModalSalle({ salle, show,setShow, setShowReserver }) {

    function toggle(reponse) {
        setShow(!show);
        if (reponse) {
            setShowReserver(true)
        }
    }

const listeMateriel = salle.listeMateriel.map((materiel) => materiel.quantite>0 ? <div>- {materiel.typeMateriel.libelle} : {materiel.quantite}</div>:'' )
    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={()=>toggle(false)}></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={()=>toggle(false)} centered>
                    <MDBModalHeader toggle={()=>toggle(false)}>
                       Détails de Salle : {salle.nom}
                    </MDBModalHeader>
                    <MDBModalBody>
                        Type : {salle.typeSalle.libelle}<br/>
                        Batiment : {salle.batiment.nom}<br/>
                        Numero : {salle.numero}<br/>
                        Etage  : {salle.etage}<br/> 
                        Capacité : {salle.capacite} personne(s)<br/>
                        Superficie : {salle.surface} m²<br/><br/>
                        Materiels présents : <br/>
                        {listeMateriel}
                    </MDBModalBody>
                    <MDBModalFooter>
                    <MDBBtn className="btnGeneralDark" onClick={() => toggle(true)} >
                            Reserver
                            </MDBBtn>

                        <MDBBtn className="btnGeneralBlanc" onClick={() => toggle(false)} >
                            Fermer
                            </MDBBtn>
                   
                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>
    );

}
