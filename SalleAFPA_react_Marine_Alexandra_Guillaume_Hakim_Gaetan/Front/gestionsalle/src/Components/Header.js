import React,{Fragment} from 'react';
import {Link} from 'react-router-dom';
import '../css/header.css'

export default function Header() {

    function deconnexionOnClick() {
        sessionStorage.clear();
    }

    return (
        
        <nav className="navBarStyle navbar navbar-expand-lg">
            <div className="d-flex flex-grow-1">
                <span className="spanTitre w-100 d-lg-none d-block">{/*<!-- hidden spacer to center brand on mobile -->*/}</span>
                <Link className="sallesAfpaTitle navbar-brand d-none d-lg-inline-block" to="/salle">
                    Salles AFPA
                    </Link>
                <Link className="navbar-brand-two mx-auto d-lg-none d-inline-block" to="#">
                    <i className="icImmeuble far fa-building"></i>
                </Link>
                <div className="w-100 text-right">
                    <button className="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
                        <span className="togglerIc fas fa-bars"></span>
                    </button>
                </div>
            </div>
            <div id="listMob" className="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
                <ul id="listMob" className="navbar-nav ml-auto flex-nowrap">
                    <li className="listMob nav-item dropdown">
                        <Link to="#" className="headerLink nav-link dropdown-toggle m-2 menu-item nav-active" data-toggle="dropdown">Salles</Link>
                        <div className="dropdown-menu dropdown-menu-sm-right ">
                       {sessionStorage.admin==="true"? <Link className="dropdown-item" to="/creationsalle">Nouvelle Salle</Link> : <div></div>}
                            <Link className="dropdown-item" to="/listesalle">Liste de salles</Link>
                            
                        </div>
                    </li>
                  
                    {sessionStorage.admin==="true" ?  <li className="listMob nav-item dropdown">
                        <Link to="#" className="headerLink nav-link dropdown-toggle m-2 menu-item nav-active" data-toggle="dropdown">Réservation</Link>
                        <div className="dropdown-menu dropdown-menu-sm-right">
                            <Link className="dropdown-item" to="/creationreservation">Nouvelle Reservation</Link>
                            <Link className="dropdown-item" to="/listereservaton/1">Liste des Reservations</Link>
                            
                        </div>
                    </li> : (<div></div>)}
                    {sessionStorage.admin==="true" ? <li className="listMob nav-item">
                        <Link to="/batiment" className="headerLink nav-link m-2 menu-item">Batiment</Link>
                    </li>: (<div></div>)}
                     {sessionStorage.admin==="true" ?  <li className="listMob nav-item">
                        <Link to="/typesalle" className=" headerLink nav-link m-2 menu-item">Type salle</Link>
                    </li> : (<div></div>)}
                     {sessionStorage.admin==="true" ?  <li className="listMob nav-item">
                        <Link to="/typemateriel" className="headerLink nav-link m-2 menu-item">Type materiel</Link>
                    </li> : (<div></div>)}
                  
                                     <li className="listMob nav-item">
                    <Link to="/" onClick={deconnexionOnClick} className="deconnexionLink nav-link m-2 menu-item"><span style={{color:'red'}} class="fa fa-power-off"></span></Link> 
    </li>


                </ul>
            </div>
        </nav>
    );
}

