import React, { Fragment } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import {useHistory} from 'react-router-dom';

export default function FormModificationUser({ user, show, setShow }) {

    const history = useHistory();


    function toggle() {
        setShow(!show);
        history.push('/admin/user')
    }

    return (<Fragment>
        {!show ? (
              <MDBModal isOpen={show} toggle={toggle}></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={toggle}>
                    <MDBModalHeader toggle={toggle}>
                        Récapitulatif informations utilisateur : 
        </MDBModalHeader>
                    <MDBModalBody>
                        Nom : {user.nom}<br/>
                        Prenom : {user.prenom}<br/>
                        Mail : {user.mail}<br/>
                        Telephone : {user.tel}<br/>
                        Date de naissance : {new Date(user.dateDeNaissance).toLocaleDateString('fr-FR')}<br/>
                        Role : {user.role.libelle}<br/>
                        Fonction : {user.fonction.libelle}<br/>
                        Login : {user.authentification.login}<br/>

                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn  onClick={toggle}>
                            OK
          </MDBBtn>

                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>);

}