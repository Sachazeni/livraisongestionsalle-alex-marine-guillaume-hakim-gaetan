import React, { useState, useRef } from 'react';
import { useToasts } from 'react-toast-notifications'
import {useHistory} from 'react-router-dom';
export default function User({ rowObjet, setRefresh,type}) {

    const { addToast } = useToasts();
    const [objet, setObjet] = useState(rowObjet);
    const refInputModif = useRef(null);
    const history = useHistory();

    async function modifier() {
        refInputModif.current.setAttribute("disabled","true");
        objet.libelle=refInputModif.current.value;
        let response = await fetch(`${process.env.REACT_APP_API_URL}`+"/"+type, {
            method: "PUT",
            body: JSON.stringify(objet),
            headers: {
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8",
                'Authorization': sessionStorage.token
            }
        })

        if (response && response.status===200) {
            addToast("la modification a été effectuée", { appearance: 'success',autoDismiss: true })
        }else if(response && response.status===401){
            addToast("authentification requise", { appearance: 'error' })
            history.push('/');
          } else {
            addToast("la modification a échouée", { appearance: 'error' })
        }

    }

    function editable() {
        refInputModif.current.removeAttribute("disabled");
        refInputModif.current.focus();
    }


    async function supprimer() {
       
     if (window.confirm("Êtes-vous sûrs de supprimer le " + type + " " + objet.libelle)) {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/`+type+"/" + objet.id, {
            method: "DELETE", 
            headers: {
                'Authorization': sessionStorage.token
            }
        
        })
            .catch(error => { console.log(error); });
            if (response && response.status === 200) {
                addToast("suppression effectuée", { appearance: 'success', autoDismiss: true })
              }else if(response && response.status===401){
                addToast("authentification requise", { appearance: 'error' })
                history.push('/');
              } else if(response && response.status===400) {
                addToast("erreur suppression, " + message, { appearance: 'error', autoDismiss: true })
              }
              else  {
                addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
              }
        setRefresh();
    }

    }

    var message = function () {
        if (type === "typemateriel") {
           
            return "ce type materiel est relié à une salle"
        } else if (type === "batiment") {
            return "ce batiment est relié à une salle"
        } else if (type === "typesalle") {
            return "ce type de salle est relié à une salle"
        }else if (type === "fonction") {
            return "cette fonction est reliée à un utilisateur"
        }
    }();

    
    return <tr text-center>
        <td  className="ligneTabRes"><input type="text" id={objet.id} className="inputGestion form-control" defaultValue={objet.libelle} ref={refInputModif} disabled onBlur={modifier} /></td>
        <td  className="ligneTabRes"><i onClick={editable} className="reservationIcon fas fa-pencil-alt"></i></td>
        <td  className="ligneTabRes"><i  onClick={supprimer} className="reservationIcon fas fa-trash-alt"></i></td>
        </tr>
}