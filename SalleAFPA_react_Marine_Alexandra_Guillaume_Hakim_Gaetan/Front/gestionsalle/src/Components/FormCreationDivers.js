import React, { useState, useRef } from 'react';
import $ from 'jquery'
import {  useToasts } from 'react-toast-notifications';
import {Link,useHistory} from 'react-router-dom';

export default function FormCreationDivers({ type, setRefresh }) {
    const history = useHistory();
    const [objet, setObjet] = useState({
        "id": 0,
        "libelle": ""
    });

    const refInput = useRef(null);
    const { addToast } = useToasts();

    function changeHandler(event) {
        let objetTemp = objet
        objet.libelle = event.target.value;
        setObjet(objetTemp)
    };


    async function submitForm(event) {
        event.preventDefault();
        if(objet.libelle!==""){
        let response = await fetch(`${process.env.REACT_APP_API_URL}/` + type, {
            method: "POST",
            body: JSON.stringify(objet),
            headers: {
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8",
                'Authorization': sessionStorage.token
            }
        })

        
        if (response && response.status===200) {
            addToast("nouveau " + titre + " créé", { appearance: 'success',autoDismiss: true })
        }else if(response && response.status===401){
            addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{ history.push('/')}})
           
          } else {
            addToast("la création a échouée", { appearance: 'error',autoDismiss: true  })
        }
        let objetTemp = objet;
        objet.libelle = "";
        setObjet(objetTemp)
        $("#libelle").val('');
        setRefresh();
    }
    };

    var titre = function(){
        if(type==="typemateriel"){
            return "Nouveau Type de Matériel"
        }else  if(type==="batiment"){
            return "Nouveau Bâtiment"
        }else if(type==="typesalle"){
            return "Nouveau Type de Salle"
        }else if(type==="fonction"){
            return "Nouvelle fonction"
        }
    }();

    var texteInput = function(){
        if(type==="typemateriel"){
            return "Type de Matériel"
        }else  if(type==="batiment"){
            return "Bâtiment"
        }else if(type==="typesalle"){
            return "Type de Salle"
        }else if(type==="fonction"){
            return "Fonction"
        }
    }();
    
    var imageParClasse= function(){
        if(type==="typemateriel"){
            return "Type de Matériel"
        }else  if(type==="batiment"){
            return "Bâtiment"
        }else if(type==="typesalle"){
            return "bg-imageTypeSalle"
        }
    }


    return <div className="">
       
        <h3 className="titreDivers"> {titre}  : </h3>
        <hr/>
        <form
            className="needs-validation "
            onSubmit={submitForm}
            noValidate
        >
            <div class="input-group mb-3">
  <input className="form-control" type="text" name="nom" valueDefault={objet.libelle}  
              type="text"
              onChange={changeHandler}
              id="libelle"
              label= {titre}
              ref={refInput}
              placeholder= {texteInput}
              required />

  <div class="input-group-append">
    <button type="submit"  id="buttonDivers" >Ajouter</button>
  </div>
</div>
           
        </form>
        
  
    </div>
}
