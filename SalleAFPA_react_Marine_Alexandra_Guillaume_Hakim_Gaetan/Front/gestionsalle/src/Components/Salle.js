import React from 'react';
import {Link,useHistory} from 'react-router-dom';

export default function Salle({ salle }) {

  const history = useHistory();
  return     <div class="card cardSalleList">
            <img class="card-img-top imgStyle" src={salle.cheminImage} alt="Card image cap" />

            <div class="card-body">

              <h4 class="card-title">{salle.nom}</h4>

              <p class="card-text">{salle.typeSalle.libelle} n° {salle.numero}<br/>
                Bâtiment : {salle.batiment.libelle} Etage : {salle.etage}<br/>
                Capacite : {salle.capacite} personne(s)<br /></p>
                <Link className="btn btnListS" to={"/salle/" + salle.idSalle}>Détails</Link>
              
            </div>
          </div>
  
}