import React, { useState, useEffect, Fragment } from 'react';
import LigneTableauDivers from './LigneTableauDivers';
import { useToasts } from 'react-toast-notifications';
import FormCreationDivers from '../Components/FormCreationDivers'
import {useParams, useHistory,Link} from 'react-router-dom' 

export default function Gestion({ type, refresh, setRefresh }) {

    const history = useHistory();

    const [listeObjets, setListeObjets] = useState([]);
    const [nbPage, setNbPage] = useState(true);

    const [isLoading, setIsLoading] = useState(true);
    const { addToast } = useToasts();
    const { page } = useParams();
    const[pageUrl,setPageUrl]=useState(page ? page : 1)

    const getObjets = async () => {
        let pageUrl = page ? page : 1;
        let response = await fetch(`${process.env.REACT_APP_API_URL}/` + type + `/${pageUrl}`, {
            headers: {
                'Authorization': sessionStorage.token
            }
        })
            .catch(error => console.log(error))

        if (response && response.status === 200) {
            let responseData = await response.json();
            setListeObjets(responseData.listeObjet);
            setNbPage(responseData.nbPage);
        } else if (response && response.status === 401) {
            addToast("authentification requise", { appearance: 'error', autoDismiss: true, autoDismissTimeout: 1000, onDismiss: (id) => { history.push("/"); } })

        } else {
            addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
        }
    }

    useEffect(() => {
        setPageUrl(page ? page : 1)
        const fetchData = async () => {
            setIsLoading(true);
            await getObjets();
            setIsLoading(false);
        }
        fetchData();
    }, [refresh, type,page]);
    
    const pagination = () =>{
        let retour=[];
        for(let i=1;i<=nbPage;i++){
            retour.push(<li className={"page-item " + (i==pageUrl ? 'active' : '')} ><a className="page-link"onClick={ ()=>history.push("/"+type+"/"+i)}>{i}</a></li>)
        }
        return retour;
    }
    const tabObjet = listeObjets.map((objet) => <LigneTableauDivers key={objet.id} rowObjet={objet} type={type} setRefresh={setRefresh} />);
   
    let image="bg-imageUser";

    var titre = function () {
        if (type === "typemateriel") {
            image="bg-imageTypeMateriel"
            return "Type Materiel"
        } else if (type === "batiment") {
            image="bg-imageBatiment";
            return "Batiment"
        } else if (type === "typesalle") {
            image="bg-imageTypeSalle";
            return "Type Salle"
        }else if (type === "fonction") {
            image="bg-imageFonction";
            return "Fonction"
        }
    }();

    return (<Fragment>
        {isLoading ? (
            <div></div>
        ) : (<div className=" container-fluid">
            <div className="row no-gutter">
                <div className={" col-md-3 d-none d-md-flex "+image+" align-items-center"}></div>
                <div className=" col-md-8 col-lg-7 ">
                    <div className="userListContainer">
                        <div className="">
                            <FormCreationDivers className="" type={type} setRefresh={setRefresh} refresh={refresh} />
                        </div>


                        <br />
                        <table className="table table-hover table-sm table-striped">

                            <tbody>{tabObjet}</tbody>

                        </table>
                        <nav aria-label="...">
            <ul className="pagination justify-content-center">
                <li className={"page-item " + ((pageUrl-1)<1 ? 'disabled' : '')}>
                <Link className="page-link" to={"/"+type+"/"+(pageUrl-1)} >&laquo;</Link>
                </li> 
                {pagination()}
                <li className={"page-item " + ((parseInt(pageUrl)+1)>nbPage ? 'disabled' : '')}>
                    <Link className="page-link" to={"/"+type+"/"+(parseInt(pageUrl)+1)}>&raquo;</Link>
                </li>
            </ul>
        </nav>
                    </div>
                </div>
            </div>

        </div>)
        }</Fragment>);

}