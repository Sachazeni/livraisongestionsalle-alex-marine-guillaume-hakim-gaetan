import React, { useState } from 'react';
import { MDBCard, MDBCol, MDBCardImage, MDBCardBody, MDBCardTitle, MDBCardText, MDBBtn } from "mdbreact";
import {Link,useHistory} from 'react-router-dom';

import imgSalle from '../img/arlington-research-Dnt2DT6wNWo-unsplash.jpg'
import { useToasts } from 'react-toast-notifications'
import ModalConf from './ModalConfirmationReservation'
import ModalDetail from './ModalDetailsSalleReservation'

export default function SalleReservation({ salle, reservation, setReservation,setLoad }) {

  const { addToast } = useToasts();
  const [show, setShow] = useState(false)
  const [showDetails, setShowDetails] = useState(false)
  const history = useHistory();

  async function reserver() {
  
      reservation.salle = salle;
      setReservation(reservation);
      let response = await fetch(`${process.env.REACT_APP_API_URL}/reservation`, {
        method: "POST",
        body: JSON.stringify(reservation),
        headers: {
          'Accept': 'application/json',
          "Content-type": "application/json; charset=UTF-8",
          'Authorization': sessionStorage.token
        }
      })
      
      if (response && response.status === 200) {
        setReservation({"id":"0",
        "dateDebut":"",
        "dateFin":"",
        "nomReservation":"",
        "salle":{}})
        setLoad(false)
        addToast("reservation effectuée", { appearance: 'success',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/listereservaton')}})

      } else if(response && response.status===401){
        addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/')}})
       
      } else if(response) {
        addToast("erreur reservation non effectuée", { appearance: 'error', autoDismiss: true })
      }else{
        addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
      }
      
  };


  return <div class="card cardSalleList col-sm-4">
    
    <img class="card-img-top imgStyle" src={imgSalle} alt="Card image cap" />
    <div class="card-body">
    <h4 class="card-title">{salle.nom}</h4>

              <p class="card-text">{salle.typeSalle.libelle} n° {salle.numero}<br />
                Bâtiment : {salle.batiment.libelle} Etage : {salle.etage}<br />
                Capacite : {salle.capacite} personne(s)</p>


        <a class="btn btnListS" onClick={() =>{setShowDetails(true);}} size="sm">Détails</a>

        <a class="btn btnListS" onClick={() => {setShow(true);}} size="sm">Reserver</a>

        </div>
   
    <ModalConf reservation={reservation} show={show} setShow={setShow} reserver={reserver} salle={salle}/>
    <ModalDetail reservation={reservation} show={showDetails}setShowReserver={setShow} setShow={setShowDetails} salle={salle}/>
    </div>
}