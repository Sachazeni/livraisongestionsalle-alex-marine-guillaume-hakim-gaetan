import React from 'react';
import { MDBIcon } from "mdbreact";

export default function LigenTableauReservation({ reservation,setReservation,setShow, setShowDetails, setShowModif}) {



    function editable() {
      setReservation(reservation);
      setShowModif(true)
    }

    function detailsAfficher() {
      setReservation(reservation);
      setShowDetails(true)
    }

    async function supprimer() {
      setReservation(reservation);
      setShow(true)
    }

    return <tr className="text-center">
        <td className="ligneTabRes" onClick={detailsAfficher} >{reservation.nomReservation}</td>
        <td className="ligneTabRes" onClick={detailsAfficher} >{new Date(reservation.dateDebut).toLocaleDateString('fr-FR')}</td>
        <td className="ligneTabRes" onClick={detailsAfficher}>{new Date(reservation.dateFin).toLocaleDateString('fr-FR')}</td>
        <td className="ligneTabRes" onClick={detailsAfficher}>{reservation.salle.nom}</td>
        <td className="ligneTabRes" onClick={detailsAfficher}>{reservation.salle.numero}</td>
        <td className="ligneTabRes" onClick={detailsAfficher}>{reservation.salle.batiment.libelle}</td>
        
        <td className="ligneTabRes" onClick={editable} title="Modifier"><MDBIcon className="reservationIcon" icon="pencil-alt" /></td>
        <td className="ligneTabRes" onClick={supprimer} title="Supprimer"><MDBIcon className="reservationIcon" far icon="trash-alt" /></td></tr>
       
}