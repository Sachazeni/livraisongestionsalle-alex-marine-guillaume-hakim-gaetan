<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssVisualisationSalle.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Visualisation</title>
</head>
<body>

<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<jsp:include page="menu.jsp" />
			<div class="container">
		
		<!--Ligne UNE-->
		<div class="ligne">
			<form action="" method="post">
				<!--COLONNE GAUCHE-->
				<div class="colonne">
					<!--partie nom-->
					<div class="colonneGauche">
						<label>Nom de la salle:</label>
					</div>
					<div class="colonneDroite">
						<label name="nom">${salle.nom}</label>
					</div>
					<!--partie prenom-->
					<div class="colonneGauche">
						<label>Type de salle :</label>
					</div>
					<div class="colonneDroite">
						<label prenom="prenom">${salle.typeSalle.libelle }</label>
					</div>
					<!--partie mail-->
					<div class="colonneGauche">
						<label>Batiment :</label>
					</div>
					<div class="colonneDroite">
						<label mail="mail">${salle.batiment.nom }</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Numero :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${salle.numero}</label>
					</div>
					<!--partie adresse-->
					<div class="colonneGauche">
						<label>Etage :</label>
					</div>
					<div class="colonneDroite">
						<label name="adresse">${salle.etage }</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Capacite :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${salle.capacite } personnes</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Superficie :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${salle.surface } m�tres carr�es</label>
					</div>
					<!--partie date-->
					<div class="colonneGauche">
						<label for="naissance">Actif :</label>
					</div>
					<div class="colonneDroite">
						<label name="date">${salle.actif }</label>
					</div>
					<c:if test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID}">
					<div class="colonneGauche">
						<a href="${Parametrage.URI_MODIFIER_SALLE}?id=${salle.idSalle}"><button name="btn" type="button" value="modifier">Modifier la salle</button></a>
					</div>
					</c:if>
				</div>
				
				
				<!--COLONNE DROITE-->
				<div class="colonne">
					<!--partie login-->
					 <div class="colonneGauche">
						<label>Liste mat�riaux </label>
					</div>
					<div class="colonneDroite">
						<label name="login"> : </label>
					</div>
					<c:forEach var= "materiel" items="${salle.listeMateriel }">
                       <div class="colonneGauche">
						<label>${materiel.typeMateriel.libelle } :</label>
					</div>
					<div class="colonneDroite">
						<label name="login">${materiel.quantite }</label>
					</div>
                           
                           </c:forEach>
                            <div class="colonneGauche">
						<label>Liste r�servations</label>
					</div>
					<div class="colonneDroite">
						<label name="login"> :</label>
					</div>
                           <c:forEach var= "reserv" items="${salle.listeReservation }">
                       <div class="colonneGauche">
						<label>${reserv.nomReservation }:</label>
					</div>
					<div class="colonneDroite">
						<label name="login">du ${reserv.dateDebut } au ${reserv.dateFin }</label>
					</div>
                           
                           </c:forEach>
					
				</div>
			</form>
		</div>
	</div>
				
					
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>

</body>
</html>