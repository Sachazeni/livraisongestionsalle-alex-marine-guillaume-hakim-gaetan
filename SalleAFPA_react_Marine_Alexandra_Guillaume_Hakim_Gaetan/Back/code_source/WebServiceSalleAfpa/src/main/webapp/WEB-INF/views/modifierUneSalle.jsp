<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/menu.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssVisualisationSalle.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Modification</title>
</head>
<body>
<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<jsp:include page="menu.jsp" />
			<div class="container">

				<!--Ligne UNE-->
				<div class="ligne">

					<form action="modifier" method="post">
						<!--COLONNE GAUCHE-->
						<div class="colonne">
							<!--partie nom-->
							<div class="colonneGauche">
								<label>Nom de la salle:</label>
							</div>
							<div class="colonneDroite">
								<input type="text" name="nomSalle" value="${salle.nom}" /> <em
									class="erreur">${nomKO}</em>

							</div>
							<!--partie prenom-->
							<div class="colonneGauche">
								<label>Type de salle :</label>
							</div>
							<div class="colonneDroite">
								<select name="typeSalle">
									<c:forEach items="${listeTypeSalle}" var="typeSalle">
										<option value="${typeSalle.id}" />
										<c:out value="${typeSalle.libelle}" />
									</c:forEach>
								</select>
							</div>

							<!--partie mail-->
							<div class="colonneGauche">
								<label>Batiment :</label>
							</div>
							<div class="colonneDroite">
								<label mail="batiment">${salle.batiment.nom }</label>
							</div>
							<!--partie telephone-->
							<div class="colonneGauche">
								<label>Numero :</label>
							</div>
							<div class="colonneDroite">
								<input type="text" name="numeroSalle" value="${salle.numero}" />
								<br> <em class="erreur">${numeroKO}</em>
							</div>
							<!--partie adresse-->
							<div class="colonneGauche">
								<label>Etage :</label>
							</div>
							<div class="colonneDroite">
								<label name="etage">${salle.etage }</label>
							</div>
							<!--partie telephone-->
							<div class="colonneGauche">
								<label>Capacite :</label>
							</div>
							<div class="colonneDroite">
								<input name="capacite" type="number" value=${salle.capacite }></label>
							</div>
							<!--partie telephone-->
							<div class="colonneGauche">
								<label>Superficie :</label>
							</div>
							<div class="colonneDroite">
								<label name="superficie">${salle.surface } m�tres
									carr�es</label>
							</div>
							<!--partie date-->
							<div class="colonneGauche">
								<label for="actif">Actif :</label>
							</div>
							<div class="colonneDroite">

								<input type="radio" name="actif" value="true"
									<c:if test="${salle.actif }"> checked </c:if>> <label>oui</label>
								<br> <input type="radio" name="actif" value="false"
									<c:if test="${!salle.actif }"> checked </c:if>> <label>non</label>

							</div>
							
						</div>


						<!--COLONNE DROITE-->
						<div class="colonne">
							<!--partie login-->
							<div class="colonneGauche">
								<label>Liste mat�riaux </label>
							</div>
							<div class="colonneDroite">
								<label name="login"> : </label>
							</div>
							<c:choose>
							<c:when test="${fn:length(salle.listeMateriel)> 0}">
								<c:forEach var="materiel" items="${salle.listeMateriel }">
									<div class="colonneGauche">
										<label>${materiel.typeMateriel.libelle } :</label>
									</div>
									<div class="colonneDroite">
										<input name="total" type="number" value="${materiel.quantite}" />
									</div>

								</c:forEach>
							</c:when>
							<c:otherwise>
								<c:forEach var="typeMateriel" items="${listeTypeMateriel }">
									<div class="colonneGauche">
										<label>${typeMateriel.libelle }</label>
									</div>
									<div class="colonneDroite">
										<input id="${typeMateriel.idTypeMateriel}"
											name="tm${typeMateriel.idTypeMateriel}" value=""
											type="number" size="3"> <br>
									</div>
								</c:forEach>
							</c:otherwise>
							</c:choose>
							<div class="colonneGauche">
								<label>Liste r�servations</label>
							</div>
							<div class="colonneDroite">
								<label name="login"> :</label>
							</div>
							<c:forEach var="reserv" items="${salle.listeReservation }">
								<div class="colonneGauche">
									<label>${reserv.nomReservation }:</label>
								</div>
								<div class="colonneDroite">
									<label name="login">du ${reserv.dateDebut } au
										${reserv.dateFin }</label>
								</div>

							</c:forEach>
							<c:if
								test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID}">
								<div class="colonneGauche">
									 <input id="id" name="id" type="hidden" value="${salle.idSalle}"/>
									<input name="valider" type="submit" value="Valider">
								</div>
							</c:if>
						</div>
					</form>
				</div>
			</div>


		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>

</body>
</html>