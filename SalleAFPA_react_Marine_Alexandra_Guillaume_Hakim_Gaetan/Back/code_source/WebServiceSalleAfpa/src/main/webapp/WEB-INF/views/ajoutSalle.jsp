<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/menu.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssVisualisationSalle.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Ajouter Salle</title>
</head>
<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<jsp:include page="menu.jsp" />
			<div class="container">

				<!--Ligne UNE-->
				<div class="ligne">
					<form action="creation" method="post">

						<!--COLONNE GAUCHE-->

						<div class="colonne">

							<!--partie nomSalle-->
							<div class="colonneGauche">
								<label>Nom de la salle:</label>
							</div>
							<div class="colonneDroite">
								<input type="text" name="nomSalle" value="${nom}"/>
								<em class="erreur">${nomKO}</em>
							</div>

							<!--partie TypeSalle-->
							<div class="colonneGauche">
								<label>Type de salle :</label>
							</div>
							<div class="colonneDroite">
								<select name ="typeSalle" >
									<c:forEach items="${listeTypeSalle}" var="typeSalle">
										<option value="${typeSalle.id}"  />
										<c:out value="${typeSalle.libelle}" />

									</c:forEach>
								</select>
							</div>

							<!--partie batiment-->
							<div class="colonneGauche">
								<label>Batiment :</label>
							</div>
							<div class="colonneDroite">
								<select name="bat">
									<c:forEach items="${listeBatiment}" var="batiment">
										<option value="${batiment.id}" />
										<c:out value="${batiment.nom}" />
										</option>
									</c:forEach>
								</select>
							</div>
							<!--partie Numero Salle-->
							<div class="colonneGauche">
								<label>Numero :</label>
							</div>
							<div class="colonneDroite">
								<input type="text" name="numeroSalle" value="${numero}"/>
								<br><em class="erreur">${numeroKO}</em>
							</div>

							<!--partie etage-->
							<div class="colonneGauche">
								<label>Etage :</label>
							</div>
							<div class="colonneDroite">
								<input type="number" name="etage" value="${etage}" />
								<br><em class="erreur">${etageKO}</em>
							</div>

							<!--partie capacite-->
							<div class="colonneGauche">
								<label>Capacite :</label>
							</div>
							<div class="colonneDroite">
								<input type="text" name="capacite"
									placeholder="Nombre de personnes" />
								<br><em class="erreur">${capaciteKO}</em>	
							</div>

							<!--partie superficie-->
							<div class="colonneGauche">
								<label>Superficie :</label>
							</div>
							<div class="colonneDroite">
								<input type="text" name="superficie"
									placeholder="En m�tres carr�es" />
								<br><em class="erreur">${superficieKO}</em>
							</div>

							<!--partie actif-->
							<div class="colonneGauche">
								<label for="actif">Salle active :</label>
							</div>
							<div class="colonneDroite">
								<input type="radio" name="actif" value="true"
									<c:if test="${salle.actif }"> checked </c:if>> <label>oui</label>
								<br> <input type="radio" name="actif" value="false"
									<c:if test="${!salle.actif }"> checked </c:if>> <label>non</label>
							</div>

						</div>



						<!--COLONNE DROITE-->
						<div class="colonne">

							<!--partie type Materiel-->
							<div class="colonneGauche">
								<label>Materiel :</label>
							</div>
							<div class="colonneDroite"></div>

							<div class="colonneDroite">
								<c:forEach var="typeMateriel" items="${listeTypeMateriel }">
									<div class="colonneGauche">
										<label>${typeMateriel.libelle } :</label>
									</div>
									<div class="colonneDroite">
										<input id="${typeMateriel.idTypeMateriel}"
											name="tm${typeMateriel.idTypeMateriel}" value=""
											type="number" size="3"> <br>
									</div>
								</c:forEach>
							</div>
							<div class="colonneGauche">	</div>
						<div class="colonneDroite">	<input name="valider" type="submit" value="Valider"></div>

						</div>
					</form>
				</div>
			</div>


		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>

</body>
</html>