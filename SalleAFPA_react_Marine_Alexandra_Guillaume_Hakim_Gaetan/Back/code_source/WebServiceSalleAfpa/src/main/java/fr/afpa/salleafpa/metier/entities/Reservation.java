package fr.afpa.salleafpa.metier.entities;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
	
	


	Integer id;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)  
	LocalDate dateDebut;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)  
	LocalDate dateFin;
	
	String nomReservation;
	
	
	Salle salle;//????


	public Reservation(int idReservation, LocalDate dateDebut, LocalDate dateFin, String nomReservation) {
		super();
		this.id = idReservation;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nomReservation = nomReservation;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Reservation) {
		return this.getId()==((Reservation)obj).getId();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getId();
	}
	
	

}
