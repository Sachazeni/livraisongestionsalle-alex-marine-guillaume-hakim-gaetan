package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.BatimentDao;
import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.entities.TypeMaterielDao;
import fr.afpa.salleafpa.dao.repositories.BatimentRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceBatimentDto;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;

@Service
public class ServiceBatimentDto implements IServiceBatimentDto {

	@Autowired
	private BatimentRepository batimentRepository;

	@Override
	public Batiment creation(Batiment batiment) {
		BatimentDao batimentDao = batimentMetierToBatimentDao(batiment);
		batimentDao = batimentRepository.save(batimentDao);
		return batimentDaoToBatimentMetier(batimentDao);
	}

	@Override
	public Batiment update(Batiment batiment) {
		BatimentDao batimentDao = batimentMetierToBatimentDao(batiment);
		batimentDao = batimentRepository.saveAndFlush(batimentDao);
		return batimentDaoToBatimentMetier(batimentDao);
	}

	@Override
	public Batiment get(Integer id) {
		Optional<BatimentDao> batimentDao = batimentRepository.findById(id);
		if (batimentDao.isPresent()) {
			return batimentDaoToBatimentMetier(batimentDao.get());
		}
		return null;
	}

	

	
	/**
	 * Methode pour la pagination du batiment
	 * @param page
	 * @param nbBatimentPage
	 * @return
	 */
	@Override
	public List<Batiment> getAll(int page, int nbBatimentPage) {
				
		Pageable sortedByLibelle = PageRequest.of(page, nbBatimentPage, Sort.by("libelle"));
		Page<BatimentDao> listeBatimentDao = batimentRepository.findAll(sortedByLibelle);
		
		return listeBatimentDao.stream().map(ServiceBatimentDto::batimentDaoToBatimentMetier).collect(Collectors.toList());
	}
	
	/**
	 * Methode pour le nombre de pages à retourner
	 */
	@Override
	public int nbPageBatiment(int nbBatimentPage) {
		Pageable sortedByLibelle = PageRequest.of(0, nbBatimentPage, Sort.by("libelle"));
		
		return batimentRepository.findAll(sortedByLibelle).getTotalPages();
	
	}
	
	@Override
	public List<Batiment> getAll() {
		List<BatimentDao> listeBatimentDao = batimentRepository.findAll();
		return listeBatimentDao.stream().map(ServiceBatimentDto::batimentDaoToBatimentMetier).collect(Collectors.toList());
	}

	@Override
	public Batiment delete(Integer id) {
		try {
		Batiment batiment = null;
		Optional<BatimentDao> batimentDao = batimentRepository.findById(id);
		if (batimentDao.isPresent()) {
			batiment = batimentDaoToBatimentMetier(batimentDao.get());
			batimentRepository.delete(batimentDao.get());
		}
		return batiment;
		}catch (Exception e) {
			return null;
		}
	}
	public static Batiment batimentDaoToBatimentMetier(BatimentDao batimentDao) {
		Batiment batiment = new Batiment();
		batiment.setId(batimentDao.getId());
		batiment.setLibelle(batimentDao.getLibelle());
		return batiment;

	}
	public static BatimentDao batimentMetierToBatimentDao(Batiment batiment) {
		BatimentDao batimentDao = new BatimentDao();
		batimentDao.setId(batiment.getId());
		batimentDao.setLibelle(batiment.getLibelle());
		List<SalleDao> listeSalleDao = new ArrayList<SalleDao>();
		batimentDao.setListeSalle(listeSalleDao);

		return batimentDao;

	}


	

}