package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Fonction;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceFonction;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
@CrossOrigin(origins = "*")
public class RestControllerFonction {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerFonction.class);

	@Autowired
	private IServiceFonction servFonction;

	@PostMapping(value = "/fonction")
	public Fonction create(@RequestBody Fonction fonction) {
		return servFonction.create(fonction);

	}

	@PutMapping(value = "/fonction")
	public Fonction update(@RequestBody Fonction fonction) {
		return servFonction.update(fonction);
	}

	/*@GetMapping(value = "/fonction/{id}")
	public Fonction getFonctionWS(@PathVariable("id") Integer id) {
		return servFonction.getFonction(id);
	}*/

	@GetMapping(value = "/fonction")
	public ResponseEntity<?> getListeRolesWS(@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				
				List<Fonction> listeFonction = servFonction.getAllFonction();
				
				if(listeFonction!=null) {
					
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(listeFonction);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
		
		
		 
	}
	
	@GetMapping(value = "/fonction/{page}")
	public  ResponseEntity<?> getListeTypeMaterielsPagination(@PathVariable("page") String page, @RequestHeader HttpHeaders header) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpStatus status;
		
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				

				int nbTypeMatPage = 1;
				if (ControleSaisie.isIntegerPositif(page)) {
					nbTypeMatPage = Integer.parseInt(page) - 1;
				}
				if (nbTypeMatPage < 0) {
					nbTypeMatPage = 0;
				}
				
				int nbPage = servFonction.nbPage(Parametrage.NB_FONCTION_PAGE);
				
				if (nbTypeMatPage > nbPage) {
					nbTypeMatPage = nbPage-1;
				}
				
				List<Fonction> listeFonction = servFonction.getAll(nbTypeMatPage, Parametrage.NB_FONCTION_PAGE);
				map.put("listeObjet", listeFonction);
				map.put("nbPage", nbPage);

				if (listeFonction != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(map);
					
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
	}

	@DeleteMapping(value = "/fonction/{id}")
	public Fonction deleteRoleWS( @PathVariable("id") Integer id) {
		return servFonction.delete(id);
	}

}
