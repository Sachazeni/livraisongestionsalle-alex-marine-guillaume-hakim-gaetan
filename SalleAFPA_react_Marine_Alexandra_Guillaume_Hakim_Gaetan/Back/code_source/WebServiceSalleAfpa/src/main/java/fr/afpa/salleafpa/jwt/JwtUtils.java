package fr.afpa.salleafpa.jwt;

import java.util.Date;

import javax.crypto.SecretKey;

import fr.afpa.salleafpa.outils.Parametrage;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JwtUtils {
	
	
    private static SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	
    public static String createJWT(String login,String role, long ttlMillis) {
    	
    	return Jwts.builder()
    			  .setSubject(login)
    			  .claim("role",role)
    			  .setIssuedAt(new Date())
    			  .setExpiration(new Date(System.currentTimeMillis() + ttlMillis))
    			  .signWith(
    			   SECRET_KEY
    			  )
    			  .compact();
	
	}
    
    public static Jwt decodeJWT(String token)   {
    	 try {
    		 return  Jwts.parserBuilder()
                .setSigningKey(SECRET_KEY)
                .build()
                .parse(token);
    	 }catch (Exception e) {
			return null;
		}

    }
    
    public static boolean isAuthentificate(String token, boolean admin) {
    	Jwt tokenDecode = decodeJWT(token);
    	if(tokenDecode!=null) {
    		if(admin) {
    			Claims claims  = (Claims) tokenDecode.getBody();
    			return (""+Parametrage.ADMIN_ID).equals(claims.get("role"));
    		}else {
    			return true;
    		}
    	}
    	return false;
    }
}
