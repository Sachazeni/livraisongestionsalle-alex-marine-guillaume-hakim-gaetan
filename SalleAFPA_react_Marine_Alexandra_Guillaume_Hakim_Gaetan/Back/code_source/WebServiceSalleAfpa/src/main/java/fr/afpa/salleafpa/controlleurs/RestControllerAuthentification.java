package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Authentification;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.iservices.IServiceAuthentification;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController
@CrossOrigin(origins = "*")
public class RestControllerAuthentification {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerAuthentification.class);

	@Autowired
	private IServiceAuthentification servAuth;

	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public ResponseEntity<?> authentifPost(@RequestBody Authentification authentification, HttpStatus status) {
		
		
		Personne persAuth = null;
		Map<String, String> map  = new HashMap<String, String>(); 

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(authentification.getLogin(), authentification.getMdp());
		if (persAuth != null) { // la pers auth est un admin ou utilisateur
			 String token = JwtUtils.createJWT(persAuth.getAuthentification().getLogin(),""+persAuth.getRole().getId(),900000L);
			 map.put("token", token);
			 map.put("admin", ""+ (persAuth.getRole().getId()==Parametrage.ADMIN_ID));
			 status =status.OK;
		} else { // authentification incorrecte
			status=status.BAD_REQUEST;
		}
		return (ResponseEntity<?>) ResponseEntity.status(status).body(map);
	}
}
