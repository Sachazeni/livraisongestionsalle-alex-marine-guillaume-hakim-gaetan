package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController

@CrossOrigin(origins = "*")
public class RestControllerBatiment {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerBatiment.class);

	@Autowired
	private IServiceBatiment servBatiment;
	
	
	/*
	@GetMapping(value = "/batiment/{id}")
	public ResponseEntity<?> getBatimentWS(@PathVariable("id") Integer id,@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				Batiment batiment =  servBatiment.get(id);
				if(batiment!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(batiment);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 

	

	}*/

	@GetMapping(value = "/batiment")
	public ResponseEntity<?> getListeBatimentsWS(@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				List<Batiment> listeBatiment = servBatiment.getAll();
				if(listeBatiment!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(listeBatiment);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 

	}
	
	@GetMapping(value = "/batiment/{page}")
	public ResponseEntity<?> getListeBatimentsPagination(@PathVariable("page") String page,@RequestHeader HttpHeaders header) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpStatus status;
		
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				
				int nbBatimentPage = 1;
				if (ControleSaisie.isIntegerPositif(page)) {
					nbBatimentPage = Integer.parseInt(page) - 1;
				}
				if (nbBatimentPage < 0) {
					nbBatimentPage = 0;
				}
				
				int nbPage = servBatiment.nbPageBatiment(Parametrage.NB_TYPE_MATERIEL_PAGE);
				
				if (nbBatimentPage > nbPage) {
					nbBatimentPage = nbPage-1;
				}
				
				List<Batiment> listeBatiment = servBatiment.getAll(nbBatimentPage, Parametrage.NB_TYPE_MATERIEL_PAGE);
				map.put("listeObjet", listeBatiment);
				map.put("nbPage", nbPage);
				
				if(listeBatiment!=null) {
					
				status = HttpStatus.OK;
				
				return ResponseEntity.status(status).body(map);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 

	}

	

	@PostMapping(value = "/batiment")
	public ResponseEntity<?> creationBatimentWS(@RequestBody Batiment batiment, @RequestHeader HttpHeaders header) {

		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				Batiment batimentCréé = servBatiment.creation(batiment);
				if(batimentCréé!=null) {
						
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(batimentCréé);
				}else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	@PutMapping(value = "/batiment")
	public ResponseEntity<?> updateBatimentWS(@RequestBody Batiment batiment, @RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				Batiment batimentUpdate = servBatiment.update(batiment);
				if (batimentUpdate != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(batimentUpdate);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("non modifié");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	
	@DeleteMapping(value = "/batiment/{id}")
	public ResponseEntity<?> deleteBatimentWS(@PathVariable("id") Integer id, @RequestHeader HttpHeaders header) {

		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				Batiment batimentSuppr = servBatiment.delete(id);
				if (batimentSuppr != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(batimentSuppr);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("non supprimé");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

}
