package fr.afpa.salleafpa.controlleurs;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.Part;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Image;
import fr.afpa.salleafpa.metier.entities.Materiel;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;
import fr.afpa.salleafpa.metier.iservices.IServiceFiltreSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;
import io.jsonwebtoken.io.IOException;

/**
 * Handles requests for the application home page.
 */
@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
@CrossOrigin(origins = "*")
public class RestControllerSalle {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerSalle.class);

	@Autowired
	private IServiceSalle servSalle;

	@Autowired
	private IServiceTypeMateriel servTypeMateriel;

	@Autowired
	private IServiceTypeSalle servTypeSalle;

	@Autowired
	private IServiceBatiment servBatiment;

	@Autowired
	private IServiceFiltreSalle servFiltreSalle;

	/**
	 * Methode pour rediriger vers la page qui va afficher les salles
	 * 
	 * @param mv      : ModelandView
	 * @param session : Session pour les verification de la personne authentifié
	 * @return : le modelAndView crée.
	 */
	@GetMapping(value = "/salle")
	public ResponseEntity<?> visualisationsalleGet(ModelAndView mv, @RequestHeader HttpHeaders header) {

		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				List<Salle> listeSalle = servSalle.getAllSalles();
				if (listeSalle != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(listeSalle);
				} else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	/**
	 * Mapping vers la page de la salle selectionné dans la page precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@GetMapping(value = "/salle/{id}")
	public ResponseEntity<?> visualisationUneSalleGet(@PathVariable("id") String id,
			@RequestHeader HttpHeaders header) {

		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				Salle salle = servSalle.getSalle(Integer.parseInt(id));
				if (salle != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(salle);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("salle inexistante");
				}

			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	/**
	 * Methode pour lancer la creation de la salle
	 * 
	 * @param mv      : model and view
	 * @param session : session en cours avec la personne authentifié
	 * @return : le model and view initialisé
	 */
	@PostMapping(value = "/salle")
	public ResponseEntity<?> ajouterSallePost(@RequestBody  Salle  salle, @RequestHeader HttpHeaders header) {

		HttpStatus status;
		Map<String, String> map = new HashMap<String, String>();
		Object reponse = null;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				

				if (!ControleSaisie.isNonVide(salle.getNom())) {
					map.put("nomKO", "*Doit être non vide");
				}
				if (salle.getNumero()==null || !ControleSaisie.isIntegerPositif("" + salle.getNumero())) {
					map.put("numeroKO", "*Doit être un chiffre positif");
				}
				if (salle.getSurface()==null ||  !ControleSaisie.isIntegerPositif("" + Math.round(salle.getSurface()))) {
					map.put("superficieKO", "*Erreur de saisie: doit être un chiffre positif");
				}
				if (salle.getCapacite()==null || !ControleSaisie.isIntegerPositif("" + salle.getCapacite())) {
					map.put("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
				}

				if (salle.getEtage()==null ||!ControleSaisie.isChiffre("" + salle.getEtage())) {
					map.put("etageKO", "*Erreur de saisie: doit être un chiffre");
				}
				salle.getListeMateriel().forEach(m -> {
					if (!ControleSaisie.isIntegerPositif("" + m.getQuantite())) {
						map.put("qtiteKO", "*Erreur de saisie: doit être un chiffre positif");
					}
				});
				

			
				
				if(salle.getBatiment().getId()==null) {
					map.put("batimentKO", "*Erreur de saisie: selectionner un batiment");
				}
				if(salle.getTypeSalle().getId()==null) {
					map.put("typeSalleKO", "*Erreur de saisie: selectionner un type de salle");
				}
				
				if (map.isEmpty()) {
					status = HttpStatus.OK;
					if("".equals(salle.getCheminImage())) {
						salle.setCheminImage("http://localhost:8080/resources/imageSalle/salle.jpg");
					}
					reponse = servSalle.createSalle(salle);
					if (reponse != null) {
						status = HttpStatus.OK;
						return ResponseEntity.status(status).body(reponse);
					} else {
						status = HttpStatus.INTERNAL_SERVER_ERROR;
						return ResponseEntity.status(status).body("erreur");
					}
				} else {
					status = HttpStatus.BAD_REQUEST;
					reponse = map;
					return ResponseEntity.status(status).body(reponse);
				}

			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unautorized");

	}
	
	
	/**
	 * Methode pour lancer la creation de la salle
	 * 
	 * @param mv      : model and view
	 * @param session : session en cours avec la personne authentifié
	 * @return : le model and view initialisé
	 */
	@PostMapping(value = "/image")
	public ResponseEntity<?> ajouterImageSallePost(@ModelAttribute  Image  image, @RequestHeader HttpHeaders header) {

		
		 String fileName =  image.getImage().getOriginalFilename();

		 Map<String, String> reponse = new HashMap<String, String>();
			HttpStatus status;
         
         File imageFile = new File("C:\\ENV\\serveurs\\apache-tomcat-9.0.30\\webapps\\resources\\imageSalle", fileName);
         status = HttpStatus.OK;
         reponse.put("cheminImage","http://localhost:8080/resources/imageSalle/"+fileName);
         try
         {
        	 image.getImage().transferTo(imageFile);
         } catch (Exception e) 
         {
             e.printStackTrace();
             status = HttpStatus.BAD_REQUEST;
         } 
         	
		return ResponseEntity.status(status).body(reponse);

	}

	/**
	 * Mapping vers la page de modification de la salle selectionné dans la page
	 * precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@PutMapping(value = "/salle")
	public ResponseEntity<?> modifierUneSalle(@RequestBody Salle salle, @RequestHeader HttpHeaders header) {

		Object reponse;
		Map<String, String> map = new HashMap<String, String>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				if (!ControleSaisie.isNonVide(salle.getNom())) {
					map.put("nomKO", "*Doit être non vide");

				}
				if (salle.getNumero()==null || !ControleSaisie.isIntegerPositif("" + salle.getNumero())) {
					map.put("numeroKO", "*Doit être un chiffre positif");
				}
				if (salle.getSurface()==null || !ControleSaisie.isIntegerPositif("" + Math.round(salle.getSurface()))) {
					map.put("superficieKO", "*Erreur de saisie: doit être un chiffre positif");
				}
				if (salle.getCapacite()==null || !ControleSaisie.isIntegerPositif("" + salle.getCapacite())) {
					map.put("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
				}

				if (salle.getEtage()==null ||!ControleSaisie.isChiffre("" + salle.getEtage())) {
					map.put("etageKO", "*Erreur de saisie: doit être un chiffre");
				}

				salle.getListeMateriel().forEach(m -> {
					if (!ControleSaisie.isIntegerPositif("" + m.getQuantite())) {
						map.put("qtiteKO", "*Erreur de saisie: doit être un chiffre positif");
					}
				});

				if (map.isEmpty()) {

					reponse = servSalle.updateSalle(salle);
					if (reponse != null) {
						status = HttpStatus.OK;
						return ResponseEntity.status(status).body(reponse);
					} else {
						status = HttpStatus.INTERNAL_SERVER_ERROR;
						return ResponseEntity.status(status).body("erreur");
					}
				} else {
					status = HttpStatus.BAD_REQUEST;
					reponse = map;
					return ResponseEntity.status(status).body(reponse);
				}

			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
	}

	/**
	 * Methode pour rediriger vers la page qui va afficher les salles
	 * 
	 * @param mv      : ModelandView
	 * @param session : Session pour les verification de la personne authentifié
	 * @return : le modelAndView crée.
	 */

	@RequestMapping(value = "/sallefiltre/{page}")
	public ResponseEntity<?> visualisationsallePost(@RequestBody LinkedHashMap<String, Object> params,
			@RequestHeader HttpHeaders header, @PathVariable Integer page) {

		HttpStatus status;
		Object reponse;
		Map<String, Object> map = new HashMap<String, Object>();
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {

				List<Salle> listeSalle = servSalle.getAllSalles();

				String actif = (String) params.get("actif");

				// filtre actif
				if ("true".equals(actif)) {
					listeSalle = servFiltreSalle.filtreActif(listeSalle, true);
				} else if ("false".equals(actif)) {
					listeSalle = servFiltreSalle.filtreActif(listeSalle, false);
				}

				// filtre date

				String date = (String) params.get("date");
				String dateDebut = (String) params.get("dateDebut");
				String dateFin = (String) params.get("dateFin");

				if ("reserve".equals(date) && !"".equals(dateDebut) && !"".equals(dateFin)) {
					listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(dateDebut),
							LocalDate.parse(dateFin), false);

				} else if ("libre".equals(date) && !"".equals(dateDebut) && !"".equals(dateFin)) {
					listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(dateDebut),
							LocalDate.parse(dateFin), true);
				}

				// filtre capacite
				String capacite = (String) params.get("capacite");
				if (!"".equals(capacite)) {
					listeSalle = servFiltreSalle.filtreCapacite(listeSalle, Integer.parseInt(capacite));
				}

				// filtre type salle
				if (params.get("tabTypeSalle") instanceof List<?>) {
					List<String> tabTypeSalle = (ArrayList<String>) params.get("tabTypeSalle");
					if (!tabTypeSalle.isEmpty()) {
						List<Integer> listeTypeSalle = tabTypeSalle.stream().map(Integer::parseInt)
								.collect(Collectors.toList());
						listeSalle = servFiltreSalle.filtreTypeSalle(listeSalle, listeTypeSalle);
					}
				}

				// filtre batiment
				if (params.get("batiment") instanceof List<?>) {
					List<String> tabTypeSalle = (ArrayList<String>) params.get("batiment");
					if (!tabTypeSalle.isEmpty()) {
						List<Integer> listeTypeSalle = tabTypeSalle.stream().map(Integer::parseInt)
								.collect(Collectors.toList());
						listeSalle = servFiltreSalle.filtreBatiment(listeSalle, listeTypeSalle);
					}
				}

				// filtre nom
				String nom = (String) params.get("nom");
				if (nom!=null && !"".equals(nom)) {
					listeSalle = servFiltreSalle.filtreNom(listeSalle, nom);
				}

				// filtre materiel
				for (Entry<String, Object> param : params.entrySet()) {
					if ("tm".equals(param.getKey().substring(0, 2)) && !"".equals(param.getValue())) {
						listeSalle = servFiltreSalle.filtreMateriel(listeSalle,
								Integer.parseInt(param.getKey().substring(2)),  Integer.parseInt(param.getValue().toString()));
					}
				}

				listeSalle = listeSalle.stream().sorted((s1,s2)-> s1.getNumero() - s2.getNumero() ).collect(Collectors.toList());
				
				int nombreTotalSalle = listeSalle.size();
				int nombrePage = (nombreTotalSalle / Parametrage.NB_SALLE_PAGE)
						+ (nombreTotalSalle % Parametrage.NB_SALLE_PAGE != 0 ? 1 : 0);
				
				if(page>nombrePage) {
					page=nombrePage;
				} 
				if(page<1) {
					page=1;
				}
				
				int debut = page * Parametrage.NB_SALLE_PAGE - Parametrage.NB_SALLE_PAGE;
				int fin = page * Parametrage.NB_SALLE_PAGE;
				
				
				
				if (fin > listeSalle.size()) {
					fin = listeSalle.size();
				}
				listeSalle = listeSalle.subList(debut, fin);
					map.put("liste", listeSalle);
					map.put("nbPage", nombrePage);

					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(map);
				}
			}
			status = HttpStatus.UNAUTHORIZED;
			return ResponseEntity.status(status).body("unauthorized");

	}

}
