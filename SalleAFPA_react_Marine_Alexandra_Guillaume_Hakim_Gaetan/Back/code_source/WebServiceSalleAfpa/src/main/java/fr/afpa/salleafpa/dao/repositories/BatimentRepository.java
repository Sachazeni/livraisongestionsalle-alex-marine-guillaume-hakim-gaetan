package fr.afpa.salleafpa.dao.repositories;



import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.salleafpa.dao.entities.BatimentDao;

public interface BatimentRepository extends JpaRepository <BatimentDao, Integer> {
	
	

}